//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _DHT_H
#define _DHT_H

#include "../types.h"

// ������ �������
#define DHT11                   11
#define DHT22                   22      // AM2302
#define DHT_MODEL               DHT11   // ������ ������� (DHT11 ��� DHT22)

// ���������, ������������ ������ ��� ������ � DHT
#define DHT_ERR_Ok              0
#define DHT_ERR_NotConnect      -1
#define DHT_ERR_BadChksum       -2

#define DHT_WaitMaxTicks        100    // ���������� ������ GPIO, ����� �������� �������, ��� ����� �����������

// ����, ����������� ����������� dht-11/21/22
#define DHT_Data_Port           PORTB
#define DHT_Data_Pin            PINB
#define DHT_Data_DDR            DDRB
#define DHT_Data_Mask           (1<<0)


// ������� ��������� �� DHT-11 5 ���� ������ � ������ dht_Buff, ��������� ����������� ���������� ������. ���������� ��� ������
signed char dht_read_data(void);
// ������� ���������� �������������� ��������� �������� dht_read_data() �������� ����������� �� DHT-11.
float dht_get_temperature(void);
// ������� ���������� �������������� ��������� �������� dht_read_data() �������� ��������� �� DHT-11.
float dht_get_humidity(void);

#endif