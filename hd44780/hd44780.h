//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _HD44780_H
#define _HD44780_H


// ������� ����������
#define HD44780_ROWS            2       // ���-�� �����
#define HD44780_COLS            16      // ���-�� ��������

// ��������� ������ � �����������
#define HD44780_4bitMode        1       // 4-��� ����� (����� 8-���)  
#define HD44780_WaitBisyFlag    1       // ����� ����� ��������� ����������� ������� (����� ����������� ��������)        
#define HD44780_ShortDelayUs    2       // �������� �������� (����� �������� E)   
#define HD44780_BisyDelayUs     50      // �������� �������� ������������ ����������� ������� (���� HD44780_WaitBisyFlag=1)   

#define CP1251                  1251
#define CP866                   866
#define SOURCE_CODEPAGE         CP1251  // ��������� ������ �������� ����� (��� ��������������� ���������)


//// ����, ����������� ����������� hd44780
// ������ RS
#define HD44780_RS_Port         PORTC
#define HD44780_RS_DDR          DDRC
#define HD44780_RS_Mask         (1<<0)
// ������ RW
#define HD44780_RW_Port         PORTC
#define HD44780_RW_DDR          DDRC
#define HD44780_RW_Mask         (1<<1)
// ������ E
#define HD44780_E_Port          PORTC
#define HD44780_E_DDR           DDRC
#define HD44780_E_Mask          (1<<2)
// ���� ������ ������������� ���������� hd44780
#define HD44780_Data_Port       PORTD
#define HD44780_Data_Pin        PIND
#define HD44780_Data_DDR        DDRD
#if HD44780_4bitMode
#define HD44780_Data_Shift      0       // ������� ����� ������������ ���� ����� �� ����� ��
#endif


void hd44780_init(void);
void hd44780_write_cmd(unsigned char Data);
void hd44780_write_data(unsigned char Data);
void hd44780_write_buff(char *pBuff, char Len);
void hd44780_clear(void);
void hd44780_goto_xy(char Row, char Col);
void hd44780_puts(char *str);
void hd44780_printf(const char *format, ...);

#endif