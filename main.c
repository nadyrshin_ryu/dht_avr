//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <ioavr.h>
#include <inavr.h>
#include "delay\delay.h"
#include "dht\dht.h"
#include "hd44780\hd44780.h"
#include "main.h"
#include <stdio.h>


#define LED_Port         PORTB
#define LED_DDR          DDRB
#define LED_Mask         (1<<5)
#define LED_ON()         LED_Port |= LED_Mask
#define LED_OFF()        LED_Port &= ~LED_Mask


float temp, rh;

void main( void )
{
  LED_DDR = LED_Mask;
  
  hd44780_init();

  while (1)
  {
    LED_ON();
    
    if (!dht_read_data())
    {
      temp = dht_get_temperature();
      rh = dht_get_humidity();
    }
    
    hd44780_goto_xy(0, 0);
    hd44780_printf("TEMP = %d * \r\nRH   = %d %% ", (char)temp, (char) rh);
    
    LED_OFF();
    delay_ms(1000);
  }
}
